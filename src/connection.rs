use std::error::Error;
use std::net::{Ipv4Addr, TcpStream};
use std::str::Utf8Error;
use std::time::Duration;

use log::trace;


#[derive(Debug)]
pub struct AdbConnection {
    pub host: Ipv4Addr,
    pub port: u32,

    pub rw_timeout: Option<Duration>,
    pub stream: TcpStream
}


#[derive(Debug)]
pub enum AdbConnectionError {
    Offline,
    NoDevices,
    MissingPackage,
    InvalidStorage,
    UnknownError(Box<dyn Error>),
    ConnectionError(std::io::Error),
    AdbError(String)
}


impl From<std::io::Error> for AdbConnectionError {
    fn from(value: std::io::Error) -> Self {
        Self::ConnectionError(value)
    }
}

impl From<Utf8Error> for AdbConnectionError {
    fn from(value: Utf8Error) -> Self {
        Self::AdbError(format!("UTF-8 Conversion Error: {}", value.to_string()))
    }
}


impl Default for AdbConnection {
    fn default() -> Self {
        trace!(target: "adb_connection", "Estabilishing connection at localhost");

        let addr = Ipv4Addr::LOCALHOST;
        let port = 5037;
        let rwtm = Some(Duration::from_secs(2));

        let addr_str = format!("{}:{}", addr.to_string().to_owned(), port.to_owned());

        let s = TcpStream::connect(addr_str).unwrap();

        s.set_read_timeout(rwtm).unwrap();
        s.set_write_timeout(rwtm).unwrap();

        trace!(target: "adb_connection", "Connection estabilished");

        AdbConnection {
            host: addr,
            port: port,

            rw_timeout: Some(Duration::from_secs(2)),
            stream: s
        }
    }
}

impl AdbConnection {
    pub fn new(host: Ipv4Addr, port: u32, rw_timeout: Duration) -> Result<AdbConnection, AdbConnectionError> {
        let s = TcpStream::connect(format!(
            "{}:{}",
            host.to_owned(),
            host.to_string().to_owned()
        ));

        trace!(target: "adb_connection", "Estabilishing connection");

        match s {
            Ok(stream) => {
                trace!(target: "adb_connection", "Connection estabilished");

                Ok(AdbConnection {
                    host, port,
                    rw_timeout: Some(rw_timeout),
                    stream
                })
            }
            Err(e) => Err(AdbConnectionError::from(e))
        }
    }

    pub fn close(&self) {
        trace!(target: "adb_connection", "Closing connection");
        self.stream.shutdown(std::net::Shutdown::Both).unwrap();
    }

    
}