use std::net::Ipv4Addr;
use std::time::Duration;

use crate::connection::AdbConnection;


pub struct AdbClient {
    pub host: Ipv4Addr,
    pub port: u32
}

impl Default for AdbClient {
    fn default() -> Self {
        AdbClient { host: Ipv4Addr::LOCALHOST, port: 5037 }
    }
}

impl AdbClient {
    pub fn create_connection(&self, rw_timeout: Option<Duration>) -> AdbConnection {
        let connection = AdbConnection::new(self.host, self.port, rw_timeout.unwrap()).unwrap();
        connection
    }

    pub fn get_devices(&self) {

    }

    pub fn get_device(&self, index: u32) {

    }

    pub fn get_device_by_serial(&self) {

    }
}